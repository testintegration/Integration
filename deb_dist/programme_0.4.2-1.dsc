Format: 3.0 (quilt)
Source: programme
Binary: python3-programme
Architecture: all
Version: 0.4.2-1
Maintainer: Grégory David <gregory.david@bts-malraux.net>
Standards-Version: 3.9.6
Build-Depends: dh-python, python3-setuptools, python3-all, debhelper (>= 9)
Package-List:
 python3-programme deb python optional arch=all
Checksums-Sha1:
 c596ad828dda842952af82be5e9c29566c88a5ec 1589 programme_0.4.2.orig.tar.gz
 bd3125db6cf4ff352e79ca8ed4721d5117fa53c8 908 programme_0.4.2-1.debian.tar.xz
Checksums-Sha256:
 5b4270442785d621949543ffa10b3eceb0ddcc71636abd7c247ca778debbb188 1589 programme_0.4.2.orig.tar.gz
 e67ec592bcdd46bfc38c0768af636c2ad43aada9b6ed88afc46d97d7c5fc1d0a 908 programme_0.4.2-1.debian.tar.xz
Files:
 55ef92182f293687e2eeb909a9f9785d 1589 programme_0.4.2.orig.tar.gz
 d14246c31e2449027aff6342426987d0 908 programme_0.4.2-1.debian.tar.xz
